import User from '../models/user';
import jwt from 'jsonwebtoken';
import config from '../../config/env';

function authenticate(req, res, next) {
    User.findOne({ email: req.body.email })
        .exec()
        .then((user) => {
            if (!user) return next();
            user.comparePassword(req.body.password, (e, isMatch) => {
                if (e) return next(e);
                if (isMatch) {
                    user.lastLogin = Date.now();
                    user.save()
                        .then((savedUser)=>{
                            savedUser.populate({ path: 'company',select: 'name' }, (err,populatedUser)=>{
                                if(err){
                                    res.status(500).json({
                                        "error": err
                                    });
                                }
                                else{
                                    req.user = populatedUser;
                                    next();
                                }
                            });
                        })
                }
                else {
                    return next();
                }
            });
        }, (e) => next(e))
}

function generateToken(req, res, next) {
    if (!req.user) return next();

    const jwtPayload = {
        _id: req.user._id,
        email: req.user.email,
        username: req.user.username,
        company: req.user.company
    };
    const jwtData = {
        expiresIn: config.jwtDuration,
    };
    const secret = config.jwtSecret;
    req.token = jwt.sign(jwtPayload, secret, jwtData);

    next();
}

function respondJWT(req, res) {
    if (!req.user) {
        res.status(401).json({
            error: 'Unauthorized'
        });
    } else {
        res.status(200).json({
            jwt: req.token,
            user: {
                _id: req.user._id,
                username: req.user.username,
                email: req.user.email,
                position: req.user.position,
                image: req.user.image,
                company: req.user.company,
                contactNumbers: req.user.contactNumbers
            }
        });
    }
}

export default { authenticate, generateToken, respondJWT };