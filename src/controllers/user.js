import User from '../models/user';
import Machine from '../models/machine';
let fs = require('fs');
let path = require('path');
//noinspection JSUnusedLocalSymbols
function load(req, res, next, id) {
    User.findById(id)
        .populate({ path: 'company',select: 'name' })
        .exec()
        .then((user) => {
            req.dbUser = user;
            return next();
        }, (e) => next(e));
}

function get(req, res) {
    return res.json(req.dbUser);
}

function create(req, res, next) {
    let imageBase64 = req.body.image;
    let ext = imageBase64.substring("data:image/".length, imageBase64.indexOf(";base64"));
    imageBase64 = imageBase64.replace(/^data:image\/\w+;base64,/, '');
    let fileName = "/img/"+((new Date()).getTime())+"."+ext;
    fs.writeFile(path.join(__dirname,"/../../..")+fileName, imageBase64, {encoding: 'base64'}, function(err){
        if(err){
            res.status(500).json({
                "error": err
            });
        }
        else{
            req.body.image=fileName;
            User.create(req.body)
                .then((savedUser) => {
                    return res.json(savedUser);
                }, (e) => next(e));
        }
    });
}

function update(req, res, next) {
    const user = req.dbUser;
    if(req.body.image){
        let imageBase64 = req.body.image;
        let ext = imageBase64.substring("data:image/".length, imageBase64.indexOf(";base64"));
        imageBase64 = imageBase64.replace(/^data:image\/\w+;base64,/, '');
        let fileName = "/img/"+((new Date()).getTime())+"."+ext;
        fs.writeFile(path.join(__dirname,"/../../..")+fileName, imageBase64, {encoding: 'base64'}, function(err){
            if(err){
                res.status(500).json({
                    "error": err
                });
            }
            else{
                req.body.image=fileName;
                Object.assign(user, req.body);
                user.save()
                    .then((savedUser) => res.sendStatus(204),
                        (e) => next(e));
            }
        });
    }
    else {
        Object.assign(user, req.body);

        user.save()
            .then((savedUser) => res.sendStatus(204),
                (e) => next(e));
    }
}

function remove(req, res, next) {
    const user = req.dbUser;
    user.softDelete((err)=>{
        if(err)
            next(err);
        else
            res.sendStatus(204);
    });
}

export default { load, get, create, update, remove };