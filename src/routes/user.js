import express from 'express';
import userCtrl from '../controllers/user';
import validate from 'express-validation';
import validations from './validation/user';
import EJP from 'express-jwt-permissions';

const router = express.Router();

router.route('/')

    /** POST /api/users - Create new user */
    .post(validate(validations.createUser),userCtrl.create);

router.route('/:userId')
/** GET /api/users/:userId - Get user */
    .get(userCtrl.get)

    /** PUT /api/users/:userId - Update user */
    .put(validate(validations.updateUser),userCtrl.update)

    /** DELETE /api/users/:userId - Delete user */
    .delete(userCtrl.remove);

/** Load user when API with userId route parameter is hit */
router.param('userId', validate(validations.getUser));
router.param('userId', userCtrl.load);

export default router;