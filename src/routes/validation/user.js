import Joi from 'joi';

export default {
    // POST /api/tasks
    createUser: {
        body: {
            username: Joi.string().regex(/^[a-zA-Z ]{2,30}$/).required(),
            email: Joi.string().regex(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).required(),
            password: Joi.string().regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/).required(),
            image: Joi.string().required(),
            position: Joi.string().regex(/^[a-zA-Z ]{2,30}$/).required(),
            company: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
            _user: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
        }
    },

    // GET-PUT-DELETE /api/tasks/:taskId
    getUser: {
        params: {
            userId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
        }
    },

    // PUT /api/tasks/:taskId
    updateUser: {
        body: {
            username: Joi.string().regex(/^[a-zA-Z ]{2,30}$/),
            email: Joi.string().regex(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
            password: Joi.string().regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/),
            image: Joi.string(),
            position: Joi.string().regex(/^[a-zA-Z ]{2,30}$/),
            company: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
            _user: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
        }
    }
};