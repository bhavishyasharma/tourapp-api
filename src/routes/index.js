import express from 'express';
import companyRoutes from './company';
import complaintRoutes from './complaint';
import machineRoutes from './machine';
import userRoutes from './user';
import visitRoutes from './visit';
import authRoutes from './auth';
let path = require('path');

const router = express.Router();


/** GET /api-status - Check service status **/
router.use('/auth', authRoutes);
router.use('/user', userRoutes);
router.get('/img/:imageFile',(req,res,next)=>{
    let imageFile = req.params.imageFile;
    res.sendFile(path.join(__dirname,"/../../../img/")+imageFile);
});
router.get('/api-status', (req, res) =>
    res.json({
        status: "ok"
    })
);

export default router;