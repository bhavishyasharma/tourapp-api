import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import mongooseAudit from 'mongoose-audit';
import mongooseSoftDelete from 'mongoose-soft-delete';
import Company from './company';

const UserSchema = new mongoose.Schema({
    username: { type: String, required: true, trim: true },
    email: { type: String, required: true, trim: true, unique: true },
    password: { type: String, required: true, trim: true },
    lastLogin: { type: Date, required: false },
    roles: [{type:String, trim:true}],
    image: { type: String, required: true, trim: true },
});

UserSchema.pre('save', function (next) {
    this.wasNew=this.isNew;
    const user = this;
    if (!user.isModified('password')) {
        return next();
    }
    bcrypt.genSalt(10, (err, salt) => {
        if (err) return next(err);
        bcrypt.hash(user.password, salt, (hashErr, hash) => {
            if (hashErr) return next(hashErr);
            user.password = hash;
            next();
        });
    });
});


UserSchema.methods.comparePassword = function (toCompare, done) {
    bcrypt.compare(toCompare, this.password, (err, isMatch) => {
        if (err) done(err);
        else done(err, isMatch);
    });
};

UserSchema.plugin(mongooseAudit);
UserSchema.plugin(mongooseSoftDelete, { select: false });
export default mongoose.model('User', UserSchema);