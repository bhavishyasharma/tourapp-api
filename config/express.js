import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import routes from '../src/routes';
import jwt from 'express-jwt';
import config from './env';
import expressValidation from 'express-validation';

const app = express();

app.use(cors());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: true }));

app.use(jwt({
    secret: config.jwtSecret,
    requestProperty: 'auth',
    getToken: function fromHeaderOrQuerystring (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    } }).unless({path: ['/api/auth/token',new RegExp('/api/img/*', 'i')]}));
app.use('/api', routes);
app.use((err, req, res, next) => {
    if (err instanceof expressValidation.ValidationError) {
        res.status(err.status).json(err);
    }
    else if (err.status >= 100 && err.status < 600) {
        res.status(err.status).json({
            status: err.status,
            message: err.message
        });
    }
    else{
        res.status(500).json({
            status: 500,
            err: err
        });
    }
});

export default app;