export default {
    env: 'development',
    db: 'mongodb://127.0.0.1/tourapp',
    port: 3000,
    jwtSecret: 'my-api-secret',
    jwtDuration: '72 hours'
};